flask-mongoengine (1.0.0-1) unstable; urgency=medium

  * Team upload.

  [ Emanuele Rocca ]
  * New upstream release.
  * d/control: move from nose to pytest. (Closes: #1018356)
  * d/watch: use github instead of pypi. The full upstream test suite is
    included in the tarballs available on github.
  * d/rules: override addopts from setup.cfg to avoid generating test
    coverage reports.
  * d/rules: skip tests requiring actual connections to mongodb.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Bug-Submit.
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on python3-flaskext.wtf.

 -- Emanuele Rocca <ema@debian.org>  Tue, 18 Oct 2022 14:06:33 +0200

flask-mongoengine (0.9.3-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Mon, 02 May 2022 20:57:36 -0400

flask-mongoengine (0.9.3-4) unstable; urgency=medium

  * Team upload.
  * Remove autopkgtests, because they require running mongodb which we don't
    have in Debian (Closes: #927762).

 -- Ondřej Nový <onovy@debian.org>  Sun, 25 Aug 2019 09:47:17 +0200

flask-mongoengine (0.9.3-3) unstable; urgency=medium

  * Team upload.
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Sat, 03 Aug 2019 21:34:15 +0200

flask-mongoengine (0.9.3-2) unstable; urgency=medium

  * Team upload.

  [ Christoph Berg ]
  * Convert repository to gbp.

  [ Adrian Vondendriesch ]
  * Fix port in debian tests (simpleapp).

 -- Christoph Berg <christoph.berg@credativ.de>  Tue, 02 Jan 2018 16:14:33 +0100

flask-mongoengine (0.9.3-1) unstable; urgency=low

  * Initial package version (Closes: #817766).

 -- Adrian Vondendriesch <adrian.vondendriesch@credativ.de>  Wed, 31 May 2017 14:20:29 +0200
